/* Some user data for testing BEGIN */
var users = 
[
	{
		username: 'kerttu@domain.com',
		role: 'salesperson',
		first_name: 'Kerttu',
		last_name: 'Tervanen',
		is_active: true,
		last_login: 'August 24, 2016, 11:12 am'
	},
	{
		username: 'teppo@domain.com',
		role: 'manager',
		first_name: 'Teppo',
		last_name: 'Tervanen',
		is_active: true,
		last_login: 'January 27, 2016, 01:12 am'
	},
	{
		username: 'janne@domain.com',
		role: 'manager',
		first_name: 'Janne',
		last_name: 'Hamppainen',
		is_active: false,
		last_login: 'September 12, 2015, 09:42 pm'
	},
	{
		username: 'ritva@domain.com',
		role: 'manager',
		first_name: 'Ritva',
		last_name: 'Remppainen',
		is_active: true,
		last_login: 'July 7, 2016, 06:54 am'
	},
	{
		username: 'samu@domain.com',
		role: 'salesperson',
		first_name: 'Samu',
		last_name: 'Herkonen',
		is_active: true,
		last_login: 'December 24, 2015, 11:45 pm'
	},
	{
		username: 'ida@domain.com',
		role: 'worker',
		first_name: 'Ida',
		last_name: 'Tyyni',
		is_active: true,
		last_login: 'July 7, 2016, 01:23 pm'
	},
]

var userRoles = 
[
	{
		name: 'Manager',
		value: 'manager'
	},
	{
		name: 'Sales person',
		value: 'salesperson'
	},
	{
		name: 'Worker',
		value: 'worker'
	},
	{
		name: 'Maintenance',
		value: 'mainenance'
	}
	
]
/* test user data END */

/* custom variables BEGIN */

var roleCollapsed = {}


/* custom variables END */

/* custom methods BEGIN */

function isEmpty(myObject) {

    for(var key in myObject) {
        if (myObject.hasOwnProperty(key)) {
            return false;
        }
    }

    return true;
}

function collapse(row, role) {
	/* Collapse/uncollapse all rows under specific role */
	
	/* update role's collapse state */
	if (roleCollapsed.hasOwnProperty(role)) {
		roleCollapsed[role] = !roleCollapsed[role];
	}
	else {
		roleCollapsed[role] = true;
	}

	$(row).parent().children('tr').each( function( index ){

		var element = $(this);

    	if (element.is('#role_content')) {
    		element.toggleClass('hidden-row');
    	}
    });
	
}

function toClass(value) {
	return value.replace(/ /g,'_');
}

function validatePattern(value, pattern) {

	if (pattern != null) {

		if (value != null) {
			var reg = new RegExp(pattern);
			return reg.test(value);
		}

		return false;
	}

	return true;
}
/* custom methods END */

/* Vue custom filters BEGIN */

Vue.filter('toClass', function(value) {
	return toClass(value);
});

Vue.filter('getRoleName', function(value) {

	for (var i=0; i < userRoles.length; i++) {

		if (userRoles[i].value == value) {
			return userRoles[i].name;
		}
	}

	return value;
});

Vue.filter('checkLastLogin', function(value){

	return value || "no logins";
});


/* Vue custom filters END */


/* Vue view models BEGIN */

/* user form */
var vmUserForm = new Vue({
	el: '#user_form',
	data: {
		selectedUser: users[0],
		newUserData: {},
		userRoles: userRoles,
		validationMessages: {
			required: 'Field required.',
			email: 'Invalid email!'
		},
	},
	computed: {

		fullName: function() {
			var firstName = '';
			var lastName = '';
			if (this.selectedUser.first_name) {
				firstName = this.selectedUser.first_name;
			}
			if (this.selectedUser.last_name) {
				lastName = this.selectedUser.last_name;
			}
			return firstName + ' ' + lastName;
		},

	},
	methods: {

		resetPw: function() {

			//alert(this.selectedUser.username + ' password reset!');
			//console.log(JSON.stringify(this.selectedUser));
			alert(this.selectedUser.first_name);
		},
		saveUser: function(event) {

			event.preventDefault();

			if (roleCollapsed[this.newUserData.role]) {

				$('.'+toClass(this.newUserData.role)).click();
			}

			/* field bugged with v-model -> have to get data manually */
			this.newUserData.is_active = $('#is_active').val();

			/* Capitalize first and lastnames' first character */
			function capitalize(value) {
				
				return value[0].toUpperCase()+value.slice(1);
			}

			this.newUserData.first_name = capitalize(this.newUserData.first_name);
			this.newUserData.last_name = capitalize(this.newUserData.last_name);

			if (isEmpty(this.selectedUser)) {
				/* NEW USER */
				var new_user = this.newUserData;
				new_user.is_active = (new_user.is_active == 'true');

				var index = vmUserList.users.length;
				vmUserList.users.$set(index, new_user);

			}
			else {
				/* EXISTING USER */
				var index = vmUserList.users.indexOf(this.selectedUser);
				this.newUserData.is_active = (this.newUserData.is_active=='true');
				vmUserList.users.$set(index, this.newUserData);
			}
			vmUserForm.$set('newUserData', {});
			vmUserForm.$set('selectedUser', {});

			
			return true
		},
		deleteUser: function() {

			var index = users.indexOf(this.selectedUser);
			if (index >= 0) {
				vmUserList.users.$remove(this.selectedUser);
			}
			else alert('ERROR: User not found');
			this.selectedUser = vmUserList.users[0];
		},
		onSubmit: function(e) {
			e.preventDefault();
			alert(e);
		}
	},
	validators: {

		email: function (val) {
		  	return /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/.test(val);
		}
	}
})

/* add user button */
var vm_add_user = new Vue({
	el: '#add_user',
	methods: {
		newUser: function() {
			vmUserForm.$set('newUserData', {});
			vmUserForm.$set('selectedUser', {});
		}
	}

})

/* user list */
var vmUserList = new Vue({
  	el: '#user_list',
  	data: {
		users: users
	},
	computed: {

		getRoles: function() {

			var roles = {};

			for (var i=0; i < this.users.length; i++) {

				var role = this.users[i].role;

				if (role in roles) {
					roles[role]++;
				}
				else roles[role] = 1;
			}

			return roles;
		},
	},
	methods: {

		selectUser: function(user) {
			/* set new selectedUser */

			var newUser = {};

			/* selected user data needs to be copied, not directly referenced */
			for (var k in user) {
		        if (user.hasOwnProperty(k)) {
		        	if (user[k] == 'true' || user[k] == 'false') {
		        		user[k] = (user[k]=='true');
		        	}
		           	newUser[k] = user[k];
		        }
		    }
			
			vmUserForm.$set('selectedUser', user);
			vmUserForm.$set('newUserData', newUser);

		},

	}
})



/* Vue view models END */